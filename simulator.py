import numpy as np

NUM_CORRECTORS = 34
NUM_BMPS = 35

class OrbitSimulator:

    orm = None
    orbit = None
    noisiness = 0.0

    def __init__(self, orig_mat, noisiness=0.0, initial_orbit_badness=1.0):
        self.orm = orig_mat
        self.orbit = np.random.standard_normal(orig_mat.shape[0]) * initial_orbit_badness
        self.noisiness = noisiness

    def iteration(self, kicks: np.ndarray) -> np.ndarray:
        # update orbit
        noise = np.random.standard_normal(self.orbit.shape) * self.noisiness
        self.orbit = self.orbit + self.orm.dot(kicks) + noise
        return self.orbit

    def orbit_mse(self) -> float:
        return np.mean(self.orbit**2)


def test_orbit_simulator():
    orm = np.random.random((NUM_BMPS, NUM_CORRECTORS))
    sim = OrbitSimulator(orm, noisiness=0.0)
    orbit0 = sim.orbit.copy()
    sim.iteration(kicks=np.zeros(NUM_CORRECTORS))
    assert (sim.orbit == orbit0).all()
    kicks = (np.random.random((NUM_CORRECTORS)) - .5) * 2
    sim.iteration(kicks=kicks)
    sim.iteration(kicks=-kicks)
    assert np.allclose(sim.orbit, orbit0)
