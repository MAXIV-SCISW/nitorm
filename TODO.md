# TODO

- [X] make a persistent to-do list
- [X] write function to evaluate a set of models/parameters
  - [X] function should output one (few) score that can be reasonably compared with others
  - [X] one idea is to to use the average (and maybe the StD) of errs (the standard deviaton of the orbit at each step)
  - [X] explore the parameter space to find the stability region
- [X] start writing the enhancer, the code that improves the estimated ORM
- [X] test/debug enhancer
  - [ ] understand why the iterative updated Pmat diverges
    - [ ] replace histories with `collections.deque(iterable,maxlen=2)` when ready
  - [ ] step by step debugging of why the ormdiff is immediately jumping up
    - [X] check if ormdiff is a good metric/test other metrics
  - [ ] improve plotting 
    - [ ] call `ax#.clear()` at each cycle for graphs (or fix size and update tehm)
    - [ ] use `im = axis.imshow()` first then update the image with `im.set_data()`
  - [ ] more diagnostic plots
    - [ ] plot determinant of orm, pmat vs time
    - [ ] plot trace of Delta_orm (=num/dem) vs time
- [ ] general code cleanup
