import dotenv
from pathlib import Path
import matplotlib.pyplot as plt
from matplotlib.colors import SymLogNorm
import numpy as np


def mat_plot(mat):
    plt.imshow(mat, norm=SymLogNorm(1))
    plt.colorbar()
    plt.show()


def rmse(real, estim):
    return np.sqrt(np.mean((real - estim)**2))


def orbit_from_file(filename: str, idx=100) -> np.ndarray:
    dataset_dir = Path(dotenv.get_key('.env', 'DATASET_DIR'))
    with h5py.File(dataset_dir / filename, 'r') as fh:
        orbit = fh['sensor_errors']['values'][idx]
        print('inside func:', orbit)
        return orbit


# FOR NOW. move to test file once we have more than 1 test FIXME
def test_orbit_from_file():
    orbit = utils.orbit_from_file(filename='test3_sen_000300.hdf5')
    print('outside func:', orbit)
    print(orbit)
    assert isinstance(orbit, np.ndarray)
    assert orbit.shape == (35,)
    assert not np.all(orbit == 0.0)
