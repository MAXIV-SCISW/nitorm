import numpy as np

class OrmEnhancer:

    def __init__(self, orig_orbit):
        self.previous_orbit = orig_orbit
        self.Pmat = None

    def update_history(self, kicks, orbit):
        # save variables we need for the next call of next_orm
        self.Pmat = self._next_Pmat(kicks)
        self.previous_orbit = orbit

    def next_orm(self, prev_orm, kicks, post_kicks_orbit) -> np.ndarray:
        if self.Pmat is None:
            self.update_history(kicks, post_kicks_orbit)
            return prev_orm
        # renaming to names from paper
        u_next = kicks
        x_next = post_kicks_orbit
        x_next2 = self.previous_orbit
        mov_next = x_next2 - x_next

        vec1 = mov_next - prev_orm.dot(u_next)
        vec2 = u_next.dot(self.Pmat)
        num = np.outer(vec1, vec2)
        den = 1 + u_next.dot( self.Pmat.dot(u_next) )

        self.update_history(kicks, post_kicks_orbit)
        return prev_orm + ( num / den )

    # INTERNAL --------------------------------------------------------

    def _next_Pmat(self, kicks) -> np.ndarray:
        if self.Pmat is None:
            # generate original P-matrix
            Umat = np.array([kicks])
            Pmat_inv = np.matmul(Umat.T, Umat)
            return np.linalg.pinv(Pmat_inv)
        else:
            # iterate from previous P-matrix
            num = self.Pmat.dot( np.outer(kicks, kicks).dot( self.Pmat))
            den = 1 + kicks.dot( self.Pmat.dot(kicks) )
            res = self.Pmat - ( num / den )
            return res
