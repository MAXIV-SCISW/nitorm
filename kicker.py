import numpy as np

class Kicker:
    def __init__(self, start_orm: np.ndarray, gain=1.0):
        self.orm = start_orm
        self.gain = gain
        self.iorm = self.inverted_orm()

    def inverted_orm(self) -> np.ndarray:
        return np.linalg.pinv(self.orm) # TODO maybe weights?

    def get_kicks(self, orbit: np.ndarray) -> np.ndarray:
        kicks = -self.iorm.dot(orbit)
        return kicks * self.gain
