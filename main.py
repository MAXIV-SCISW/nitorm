import numpy as np
import matplotlib.pyplot as plt
from simulator import OrbitSimulator
from kicker import Kicker
from enhancer import OrmEnhancer
import utils
import h5py


class Nitorm:

    def __init__(
            self,
            orm_badness,
            real_orm=None,
            gain=1,
            machine_noisiness=0.001,
            orbit_badness=1,
            enhance=True
    ):
        if real_orm is None:
            real_orm = np.random.random((5, 5))
        bad_orm = real_orm * np.random.normal(loc=1, scale=orm_badness, size=real_orm.shape)
        self.machine = OrbitSimulator(real_orm, machine_noisiness, orbit_badness)
        self.kicker = Kicker(bad_orm, gain=gain)
        self.enhancer = OrmEnhancer(self.machine.orbit) if enhance else None
        self.orbit_errors = []
        self.orm_rel_errors = []
        self.orm_rmse = []

    def run(self, iterations=1000, iteration_callback=None):
        for i in range(iterations):
            kicks = self.kicker.get_kicks(self.machine.orbit)
            self.machine.iteration(kicks)
            if self.enhancer is not None:
                self.kicker.orm = self.enhancer.next_orm(self.kicker.orm, kicks, self.machine.orbit)

            # logging and debugging
            self.orbit_errors.append(self.machine.orbit_mse())
            self.orm_rel_errors.append(np.mean(np.abs(self.machine.orm - self.kicker.orm) / (self.machine.orm)))
            self.orm_rmse.append(utils.rmse(self.machine.orm, self.kicker.orm))
            if iteration_callback:
                iteration_callback(i, self)


def realtime_matrix_plot_callback(i, nitorm):
        kicker = nitorm.kicker
        machine = nitorm.machine
        orm_rel_errors = nitorm.orm_rel_errors
        orbit_errors = nitorm.orbit_errors
        grid = (2,3)
        plt.subplot(*grid, 1)
        plt.title(f'estimated orm {i}')
        plt.imshow(kicker.orm)
        plt.colorbar()
        plt.subplot(*grid, 2)
        plt.title('relative diff')
        plt.imshow(np.abs(kicker.orm - machine.orm)/machine.orm)
        plt.colorbar()
        plt.subplot(*grid, 3)
        plt.title('pmat')
        plt.imshow(kicker.enhancer.Pmat)
        plt.colorbar()
        plt.subplot(*grid, 4)
        plt.title('mean ormdiff history')
        plt.plot(orm_rel_errors)
        plt.subplot(*grid, 5)
        plt.title('orbit error')
        plt.plot(orbit_errors)
        plt.draw()

def experiment_error_over_time():
    for run_idx in range(20):
        nitorm = Nitorm(
            # real_orm=np.loadtxt('./mat/R.txt'),
            orm_badness=0.01,
            gain=0.15,
            orbit_badness=0.01,
            enhance=False
            )
        # try:
        #     nitorm.machine.orbit = orbit_from_file('test3_sen_000272.hdf5')
        # except Exception as e:
        #     print(e)
        #     print('Failed to read initial orbit. did you create your .env with the DATASET_DIR key?')
        #     return
            
        print("initial orbit:", nitorm.machine.orbit)
        nitorm.run()
        plt.subplot(4, 5, run_idx+1)
        plt.title("orbit errors")
        plt.plot(nitorm.orbit_errors)
    plt.show()

def experiment_orm_enhancement():
    for run_idx in range(20):
        nitorm = Nitorm(
            real_orm=np.random.random((10,10)),
            orm_badness=0.05,
            gain=0.15,
            orbit_badness=0.01,
            machine_noisiness=0.1,
            enhance=True
            )
        orig_rmse = utils.rmse(nitorm.machine.orm, nitorm.kicker.orm)
        nitorm.run()
        plt.subplot(4, 5, run_idx+1)
        plt.title("orm error")
        plt.plot(nitorm.orm_rmse)
        plt.axhline(orig_rmse, 0, 1, color='red')
    plt.show()



if __name__ == "__main__":
    experiment_orm_enhancement()
